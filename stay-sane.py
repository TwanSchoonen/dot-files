#!/usr/bin/python
# The idea of this script is to stay sane

import subprocess
from os import listdir

print("####################################################################")
print("# Compare installed packages with the listed packages in bootstrap #")
print("####################################################################\n")

base_pkgs = subprocess.check_output(
    ["pacman", "-Sg", "base-devel"]).decode("UTF-8").splitlines()
base_pkgs = [x.split()[1] for x in base_pkgs]

blacklist_pkgs = [
    # Preinstalled
    "base",
    "git",
    "intel-ucode",
    "linux",
    "linux-firmware",
    "linux-headers",

    # Networking
    "wpa_supplicant",
    "netctl",
    "dialog",
    "dhcpcd",

    # Alternatively installed
    "emacs",
    "emacs-native-comp-git",
    "emacs27-git",
    "yay",
    "reflector",
]

installed_pkgs = subprocess.check_output(
    ["pacman", "-Qeq"]).decode("UTF-8").splitlines()

bootstrap_pkgs = []
with open("/home/t/.config/dot-files/bootstrap.sh") as f:
    for line in f:
        if line.strip() == "echo Installing packages":
            while "yay" not in line:
                line = f.readline()
                if '"' in line:
                    bootstrap_pkgs.extend(line.split('"')[1].split())
            break

for pkg in installed_pkgs:
    if pkg not in bootstrap_pkgs \
       and pkg not in base_pkgs \
       and pkg not in blacklist_pkgs \
       and "texlive" not in pkg:
        print("-", pkg, "is not listed")

print("\n###########################################")
print("# Preform sanity check on the home folder #")
print("###########################################\n")

sane_home_folders = [
    # Sane folders
    "Pictures",
    "Dropbox",
    "Documents",
    "Downloads",
    "Git",
    "mnt",

    # Sane hidden folders
    ".local",
    ".mail",
    ".config",
    ".gnupg",
    ".ssh",
    ".dropbox",
    ".dropbox-dist",
    ".cache",
    ".subversion",
    ".android",

    # Sane files
    ".zprofile",
    ".hunspell_en_GB",

    # Less sane
    ".lein",
    ".java",
    ".mozilla",
    ".python_history",
    ".thunderbird",
    ".m2",
    ".pki",
    ".node_repl_history",
]

for home_folder in listdir("/home/t"):
    if home_folder not in sane_home_folders:
        print("-", home_folder, "is non sane")

print("")

for sane_home_folder in sane_home_folders:
    if sane_home_folder not in listdir("/home/t"):
        print("-", sane_home_folder, "listed but not present")

print("\n#############################################")
print("# Preform sanity check on the config folder #")
print("#############################################\n")

sane_config_folders = [
    # Sane config folders
    "gtk-3.0",
    "gtk-2.0",
    "git",
    "dunst",
    "emacs",
    "X11",
    "mbsync",
    "zsh",
    "dot-files",
    "xprofile",
    "systemd",
    "transmission-daemon",
    "freerdp",
    "remmina",

    # Sane config files
    "user-dirs.dirs",
    "mimeapps.list",

    # Less sane
    "clojure",
    "libreoffice",
    "wireshark",
    "procps",
    "autostart",
    "GIMP",
    "dconf",
    "yay",
    "spotify",
    "pavucontrol.ini",
    "pulse",
    "QtProject.conf",
    "htop",
    "matplotlib",
    "Signal",
    "Electron",
    "mpv",
]

for config_folder in listdir("/home/t/.config"):
    if config_folder not in sane_config_folders:
        print("-", config_folder, "is non sane")

print("")

for sane_config_folder in sane_config_folders:
    if sane_config_folder not in listdir("/home/t/.config"):
        print("-", sane_config_folder, "listed but not present")

print("\n############################################")
print("# preform sanity check on the cache folder #")
print("############################################\n")

sane_cache_folders = [
    "clojure",
    "orchard",  # For clojure
    "mozilla",
    "jedi",
    "dmenu_run",
    "transmission",
    "mu",
    "yay",
    "mesa_shader_cache",
    "spotify",
    "thunderbird",
    "gimp",
    "fontconfig",
    "matplotlib",
    "pip",
    "go-build",
    "vim",
    "thumbnails",
    "mirrorstatus.json",
]

for cache_folder in listdir("/home/t/.cache"):
    if cache_folder not in sane_cache_folders:
        print("-", cache_folder, "is non sane")

print("")

for sane_cache_folder in sane_cache_folders:
    if sane_cache_folder not in listdir("/home/t/.cache"):
        print("-", sane_cache_folder, "listed but not present")

print("\n##################################################")
print("# preform sanity check on the local/share folder #")
print("##################################################\n")

sane_local_share_folders = [
    "applications",
    "systemd",
    "Trash",
    "xorg",
    "password-store",
    "virtualenv",
    "recently-used.xbel",
    "go",
]

for local_share_folder in listdir("/home/t/.local/share"):
    if local_share_folder not in sane_local_share_folders:
        print("-", local_share_folder, "is non sane")

print("")

for sane_local_share_folder in sane_local_share_folders:
    if sane_local_share_folder not in listdir("/home/t/.local/share"):
        print("-", sane_local_share_folder, "listed but not present")

print("\n################################")
print("# Preform sanity check on root #")
print("################################\n")

# passwd = subprocess.Popen(('pass', 'nsip'), stdout=subprocess.PIPE)
# lostfiles = subprocess.check_output(
#     ["sudo", "-S", "lostfiles", "-s"], stdin=passwd.stdout).decode("UTF-8").splitlines()

lostfiles = subprocess.check_output(
    ["sudo", "lostfiles", "-s"]).decode("UTF-8").splitlines()

sane_root_files = [
    # Boot config
    "/boot/c09025cdcfdf4d2f970ff6d2a5910642",
    "/boot/vmlinuz-linux",
    "/boot/EFI",
    "/boot/EFI/BOOT",
    "/boot/EFI/BOOT/BOOTX64.EFI",
    "/boot/EFI/Linux",
    "/boot/EFI/systemd",
    "/boot/EFI/systemd/systemd-bootx64.efi",
    "/boot/initramfs-linux-fallback.img",
    "/boot/initramfs-linux.img",
    "/boot/loader",
    "/boot/loader/entries",
    "/boot/loader/entries/arch.conf",
    "/boot/loader/loader.conf",
    "/boot/loader/random-seed",

    # My files and folders
    "/etc/pacman.d/hooks",
    "/etc/pacman.d/hooks/100-systemd-boot.hook",
    "/etc/pacman.d/hooks/nvidia.hook",
    "/etc/udev/rules.d/99-lowbat.rules",
    "/etc/systemd/system/kbdrate.service",
    "/etc/vconsole.conf",
    "/etc/locale.conf",
    "/etc/pacman.d/hooks/mirrorupgrade.hook",
    "/etc/sysctl.d/laptop.conf",
    "/usr/share/kbd/keymaps/personal.map",
    "/etc/systemd/system/sleep@.service",
    "/etc/sysctl.d/99-swapiness.conf",

    "/etc/group-",
    "/etc/gshadow-",
    "/etc/passwd-",
    "/etc/shadow-",
    "/etc/hostname",
    "/etc/adjtime",
    "/etc/ld.so.cache",
    "/etc/localtime",
    "/etc/machine-id",
    "/etc/os-release",
    "/etc/.pwd.lock",
    "/etc/mkinitcpio.d/linux.preset",

    # Networking
    "/etc/netctl/webbvpn",
    "/etc/netctl/wlp4s0-Goudsberg2017",
    "/etc/netctl/wlp4s0-HOLTHAUSENGAST",
    "/etc/netctl/wlp4s0-RouterP190n5G",
    "/etc/netctl/wlp4s0-WebbNet2_5G",
    "/etc/netctl/wlp4s0-WiFi in de trein",
    "/etc/netctl/wlp4s0-devolo-187",
    "/etc/netctl/wlp4s0-Klippen op centje",
    "/etc/netctl/wlp4s0-RouterP190a",
    "/etc/netctl/wlp4s0-RouterP190n",
    "/etc/netctl/wlp4s0-uwAdvertentieHadHierKunnenStaan",
    "/etc/netctl/wlp4s0-Ziggo19F41F7",
    "/etc/netctl/wlp4s0-Beta OPEN",
    "/etc/systemd/system/dbus-org.freedesktop.timesync1.service",
    "/etc/systemd/system/sys-subsystem-net-devices-wlp4s0.device.wants",
    "/etc/systemd/system/sys-subsystem-net-devices-wlp4s0.device.wants/netctl-auto@wlp4s0.service",
    "/var/lib/systemd/timesync",
    "/var/lib/systemd/timesync/clock",

    # Firefox legacy
    "/usr/lib/firefox/config.js",
    "/usr/lib/firefox/defaults/pref/config-prefs.js",
    "/usr/lib/firefox/legacy",
    "/usr/lib/firefox/legacy/BootstrapLoader.jsm",
    "/usr/lib/firefox/legacy.manifest",
    "/usr/lib/firefox/legacy/RDFDataSource.jsm",
    "/usr/lib/firefox/legacy/RDFManifestConverter.jsm",

    # st
    "/usr/local/bin/st",
    "/usr/share/terminfo/s/st-bs",
    "/usr/share/terminfo/s/st-bs-256color",
    "/usr/share/terminfo/s/st-meta",
    "/usr/share/terminfo/s/st-meta-256color",
    "/usr/share/terminfo/s/st-mono",
    "/usr/local/man/man1/st.1",

    # dwm
    "/usr/local/bin/dwm",
    "/usr/local/man/man1",
    "/usr/local/man/man1/dwm.1",

    # dmenu
    "/usr/local/bin/dmenu",
    "/usr/local/bin/dmenu_path",
    "/usr/local/bin/dmenu_run",
    "/usr/local/man/man1/dmenu.1",

    "/usr/local/bin/stest",
    "/usr/local/man/man1/stest.1",

    # cups
    "/etc/printcap",
    "/etc/cups/classes.conf.O",
    "/etc/cups/printers.conf.O",
    "/etc/cups/ssl/35EEE1000000.local.crt",
    "/etc/cups/subscriptions.conf.O",
    "/var/cache/cups/Canon-MG7700-series.strings",
    "/var/cache/cups/Canon_MG7700_series.strings",
    "/var/cache/cups/help.index",
    "/var/cache/cups/job.cache",
    "/var/cache/cups/job.cache.O",
    "/var/cache/cups/org.cups.cupsd",
    "/var/cache/cups/ppds.dat",
    "/var/log/cups/access_log",
    "/var/log/cups/error_log",
    "/var/log/cups/page_log",
    "/var/log/samba/log.samba",
    "/var/lib/samba/private/smbd.tmp",
    "/var/spool/cups/c00020",
    "/var/spool/cups/c00021",
    "/var/spool/cups/c00022",
    "/var/spool/cups/c00023",
    "/var/spool/cups/d00007-001",
    "/var/spool/cups/d00011-001",
    "/var/spool/cups/d00012-001",
    "/var/spool/cups/d00013-001",
    "/var/spool/cups/d00014-001",
    "/var/spool/cups/d00019-001",
    "/var/spool/cups/tmp/0073260690e42",
    "/var/spool/cups/tmp/007326073ef05",

    # ssh host
    "/etc/ssh/ssh_host_dsa_key",
    "/etc/ssh/ssh_host_dsa_key.pub",
    "/etc/ssh/ssh_host_ecdsa_key",
    "/etc/ssh/ssh_host_ecdsa_key.pub",
    "/etc/ssh/ssh_host_ed25519_key",
    "/etc/ssh/ssh_host_ed25519_key.pub",
    "/etc/ssh/ssh_host_rsa_key",
    "/etc/ssh/ssh_host_rsa_key.pub",

    # certificates
    "/etc/ca-certificates/extracted/ca-bundle.trust.crt",
    "/etc/ca-certificates/extracted/cadir",
    "/etc/ca-certificates/extracted/edk2-cacerts.bin",
    "/etc/ca-certificates/extracted/email-ca-bundle.pem",
    "/etc/ca-certificates/extracted/objsign-ca-bundle.pem",
    "/etc/ca-certificates/extracted/tls-ca-bundle.pem",

    # Systemd timestamps
    "/var/.updated",
    "/etc/.updated",

    # Log files
    "/var/log/btmp",
    "/var/log/haskell-register.log",
    "/var/log/lastlog",
    "/var/log/pacman.log",
    "/var/log/private",
    "/var/log/tallylog",
    "/var/log/wtmp",
    "/var/log/Xorg.0.log",
    "/var/log/Xorg.0.log.old",
    "/var/log/Xorg.1.log",
    "/var/log/Xorg.1.log.old",

    # Lib files
    "/var/lib/dbus",
    "/var/lib/dbus/machine-id",
    "/var/lib/geoclue",
    "/var/lib/machines",
    "/var/lib/transmission",
    "/var/lib/mpd",
    "/var/lib/mpd/playlists",
    "/var/lib/pacman/sync",
    "/var/lib/pacman/sync/community.db",
    "/var/lib/pacman/sync/core.db",
    "/var/lib/pacman/sync/extra.db",
    "/var/lib/pacman/sync/multilib.db",
    "/var/lib/portables",
    "/var/lib/private",
    "/var/lib/colord",
    "/var/lib/colord/icc",
    "/var/lib/colord/mapping.db",
    "/var/lib/colord/storage.db",
    "/var/lib/systemd/pstore",
    "/var/lib/systemd/backlight",
    "/var/lib/systemd/backlight/pci-0000:00:02.0:backlight:intel_backlight",
    "/var/lib/systemd/catalog",
    "/var/lib/systemd/catalog/database",
    "/var/lib/systemd/linger",
    "/var/lib/systemd/random-seed",
    "/var/lib/systemd/rfkill",
    "/var/lib/systemd/rfkill/pci-0000:00:14.0-usb-0:8:1.0:bluetooth",
    "/var/lib/systemd/rfkill/pci-0000:04:00.0:wlan",
    "/var/lib/systemd/timers",
    "/var/lib/systemd/timers/stamp-man-db.timer",
    "/var/lib/systemd/timers/stamp-shadow.timer",

    # Systemd files
    "/etc/systemd/user/sockets.target.wants",
    "/etc/systemd/user/sockets.target.wants/dirmngr.socket",
    "/etc/systemd/user/sockets.target.wants/gpg-agent-browser.socket",
    "/etc/systemd/user/sockets.target.wants/gpg-agent-extra.socket",
    "/etc/systemd/user/sockets.target.wants/gpg-agent.socket",
    "/etc/systemd/user/sockets.target.wants/gpg-agent-ssh.socket",
    "/etc/systemd/user/sockets.target.wants/p11-kit-server.socket",
    "/etc/systemd/user/sockets.target.wants/pulseaudio.socket",

    "/etc/systemd/system/getty.target.wants",
    "/etc/systemd/system/getty.target.wants/getty@tty1.service",

    "/etc/systemd/system/multi-user.target.wants",
    "/etc/systemd/system/multi-user.target.wants/kbdrate.service",
    "/etc/systemd/system/multi-user.target.wants/tlp.service",
    "/etc/systemd/system/multi-user.target.wants/remote-fs.target",
    "/etc/systemd/system/multi-user.target.wants/nftables.service",

    "/etc/systemd/system/network-online.target.wants",
    "/etc/systemd/system/network-online.target.wants/netctl-wait-online.service",

    "/etc/systemd/system/sleep.target.wants",
    "/etc/systemd/system/sleep.target.wants/sleep@t.service",

    "/etc/systemd/system/sysinit.target.wants",
    "/etc/systemd/system/sysinit.target.wants/systemd-timesyncd.service",

    # Latex
    "/etc/texmf/ls-R",
    "/etc/texmf/web2c/updmap.cfg",
    "/usr/share/texmf-dist/ls-R",

    # Graphviz
    "/usr/lib/graphviz/config6",

    # Fonts
    "/usr/share/fonts/adobe-source-code-pro/fonts.dir",
    "/usr/share/fonts/adobe-source-code-pro/fonts.scale",
    "/usr/share/fonts/cantarell/fonts.dir",
    "/usr/share/fonts/cantarell/fonts.scale",
    "/usr/share/fonts/gnu-free/fonts.dir",
    "/usr/share/fonts/gnu-free/fonts.scale",
    "/usr/share/fonts/gsfonts/fonts.dir",
    "/usr/share/fonts/gsfonts/fonts.scale",
    "/usr/share/fonts/liberation/fonts.dir",
    "/usr/share/fonts/liberation/fonts.scale",
    "/usr/share/fonts/TTF/fonts.dir",
    "/usr/share/fonts/TTF/fonts.scale",
    "/etc/fonts/conf.d/10-hinting-slight.conf",
    "/etc/fonts/conf.d/10-scale-bitmap-fonts.conf",
    "/etc/fonts/conf.d/20-unhint-small-vera.conf",
    "/etc/fonts/conf.d/30-metric-aliases.conf",
    "/etc/fonts/conf.d/30-win32-aliases.conf",
    "/etc/fonts/conf.d/40-nonlatin.conf",
    "/etc/fonts/conf.d/45-generic.conf",
    "/etc/fonts/conf.d/45-latin.conf",
    "/etc/fonts/conf.d/49-sansserif.conf",
    "/etc/fonts/conf.d/50-user.conf",
    "/etc/fonts/conf.d/51-local.conf",
    "/etc/fonts/conf.d/60-generic.conf",
    "/etc/fonts/conf.d/60-latin.conf",
    "/etc/fonts/conf.d/65-fonts-persian.conf",
    "/etc/fonts/conf.d/65-nonlatin.conf",
    "/etc/fonts/conf.d/69-unifont.conf",
    "/etc/fonts/conf.d/69-urw-bookman.conf",
    "/etc/fonts/conf.d/69-urw-c059.conf",
    "/etc/fonts/conf.d/69-urw-d050000l.conf",
    "/etc/fonts/conf.d/69-urw-fallback-backwards.conf",
    "/etc/fonts/conf.d/69-urw-fallback-generics.conf",
    "/etc/fonts/conf.d/69-urw-fallback-specifics.conf",
    "/etc/fonts/conf.d/69-urw-gothic.conf",
    "/etc/fonts/conf.d/69-urw-nimbus-mono-ps.conf",
    "/etc/fonts/conf.d/69-urw-nimbus-roman.conf",
    "/etc/fonts/conf.d/69-urw-nimbus-sans.conf",
    "/etc/fonts/conf.d/69-urw-p052.conf",
    "/etc/fonts/conf.d/69-urw-standard-symbols-ps.conf",
    "/etc/fonts/conf.d/69-urw-z003.conf",
    "/etc/fonts/conf.d/80-delicious.conf",
    "/etc/fonts/conf.d/90-synthetic.conf",

    # Unknown
    "/usr/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache",
    "/usr/lib/gio/modules/giomodule.cache",
    "/usr/lib/gtk-2.0/2.10.0/immodules.cache",
    "/usr/lib/gtk-3.0/3.0.0/immodules.cache",
    "/usr/lib/udev/hwdb.bin",
    "/usr/share/glib-2.0/schemas/gschemas.compiled",
    "/usr/share/icons/Adwaita/icon-theme.cache",
    "/usr/share/icons/hicolor/icon-theme.cache",
    "/var/cache/ldconfig",
    "/var/cache/ldconfig/aux-cache",
    "/var/cache/private",
    "/usr/lib/locale/locale-archive",
    "/usr/share/applications/mimeinfo.cache",
    "/usr/share/info/dir",
    "/var/db/sudo/lectured/t",
]

sane_root_folders = [
    "/var/lib/pacman/local",
    "/var/lib/dhcpcd",
    "/var/lib/texmf/",

    "/var/cache/fontconfig/",

    "/etc/pacman.d/gnupg",
    "/var/cache/pacman/pkg",
    "/var/cache/man",

    "/usr/lib/ghc-",

    "/etc/ssl/certs/",
    "/etc/ca-certificates/extracted/cadir/",

    "/usr/share/mime/",
    "/var/log/journal/",
    "/var/tmp/",
    "/var/lib/systemd/coredump",
]

for lostfile in lostfiles:
    continue_flag = False
    for sane_root_folder in sane_root_folders:
        if sane_root_folder in lostfile:
            continue_flag = True
    if not continue_flag and lostfile not in sane_root_files:
        print(lostfile)

for sane_root_file in sane_root_files:
    if sane_root_file not in lostfiles:
        print(sane_root_file, "listed but not present (anymore)")
