#!/bin/sh
# Bootstraps my system
# - Keep this as minimal as possible
# - Minimize interaction with the user
# Also possible to run again on failure (it checks if steps are already done)
# Install git, install emacs and install all basic packages
# Futhermore untangle the dotfile configurations

install_needed()
{
    command -v "$1" > /dev/null \
        || (echo Installing "$1" \
	            && sudo pacman --noconfirm --needed -S "$1")
}

set_needed()
{
    [ -z "$1" ] || export "$1=$2"
}

echo Getting permission to sudo
sudo echo > /dev/null

echo Setting environmental variables
set_needed XDG_CONFIG_HOME "$HOME/.config"
set_needed XDG_DATA_HOME "$HOME/.local/share"
set_needed XDG_BIN_HOME "$HOME/.local/share"
set_needed ZDOTDIR "$XDG_CONFIG_HOME/zsh"

echo Updating repositories
sudo pacman --noconfirm --needed -Syu

install_needed reflector
[ -f "/etc/pacman.d/hooks/mirrorupgrade.hook" ] || \
    (echo "Sorting mirrorlist for the fastest hosts" && \
         reflector --latest 200 --age 24 --sort rate --save /etc/pacman.d/mirrorlist; rm -f /etc/pacman.d/mirrorlist.pacnew)

install_needed git

command -v yay > /dev/null \
    || (echo Installing yay \
	    && git clone https://aur.archlinux.org/yay /tmp/yay \
	    && cd /tmp/yay \
	    && makepkg -si --noconfirm)

echo Installing packages
sys="simple-mtpfs tlp man-db man-pages xdg-utils htop lsof cups cups-pdf"
pacman="pacman-contrib pacutils"
shells="dash zsh zsh-completions zsh-syntax-highlighting zsh-autosuggestions"
audio="alsa-utils pulseaudio pulseaudio-alsa"
net="openssh sshfs nftables nmap subversion transmission-cli freerdp gnu-netcat openconnect openresolv remmina rsync"
mail="mu isync thunderbird"
spelling="hunspell hunspell-de hunspell-nl hunspell-en_gb hunspell-en_us languagetool wordnet-common wordnet-cli"
emacs="emacs texlive-most irony-mode poppler-glib python-virtualenv python-pygments a2ps"
wm="xorg-server xorg-xinit arandr openbox"
de="pass slock redshift unclutter pavucontrol"
laptop="brightnessctl batify"
looks=""
fonts="ttf-inconsolata noto-fonts-emoji ttf-liberation ttf-all-the-icons"
apps="firefox libreoffice-fresh mpv dropbox gimp imagemagick signal-desktop spotify wireshark-qt krita tiled dunst rxvt-unicode"
games="lutris steam wine-gecko wine-mono wine-staging winetricks"
lib="sfml rapidxml"
dev="python python-pip plantuml graphviz clojure leiningen nodejs gdb cmake global sbcl clang vim android-tools ctags go gopls icmake heimdall"
etc="zip unzip unrar mpd mpc lostfiles the_silver_searcher"
linters="shellcheck flake8 minted"
graphics="mesa vulkan-intel nvidia nvidia-prime nvidia-settings"
yay --noconfirm --needed -Sy $sys $pacman $shells $audio $net $mail $spelling $emacs $wm $de $laptop $looks $fonts $apps $games $lib $dev $etc $linters $graphics

readlink -f /bin/sh | grep -q dash \
    || (echo "link /bin/sh to dash" \
	    && sudo ln -sf dash /bin/sh)

echo "$SHELL" | grep -q zsh \
    || (echo make zsh the default shell \
	    && chsh -s /bin/zsh "$USER")

# echo Cloning Emacs configuration...
# git clone https://gitlab.com/TwanSchoonen/dotemacs.git "$XDG_CONFIG_HOME/emacs"

#echo Tangling dotfiles
#cd $XDG_CONFIG_HOME/dotfiles
#emacs --batch \
#      --eval "(require 'org)" \
#      --eval '(setq org-confirm-babel-evaluate nil)' \
#      --eval '(defmacro user-emacs-file (file) (expand-file-name file user-emacs-directory))' \
#      --eval '(defmacro user-home-file (file) (expand-file-name file (getenv "HOME")))' \
#      --eval '(defmacro user-config-file (file) (expand-file-name file  (getenv "XDG_CONFIG_HOME")))' \
#      --eval '(org-babel-tangle-file "twan-literate-dotfiles.org")'
#

# echo Blocking hosts
# git clone https://github.com/StevenBlack/hosts.git "$HOME/Git"
# systemctl --user start hosts
# systemctl --user enable --now hosts.timer
# sudo ln -sf /home/t/Git/hosts/hosts /etc/hosts

#
## echo Generating dump image for Emacs...
#emacs --batch -q -l "$XDG_CONFIG_HOME"/emacs/lisp/pdumper.el

# Email setup
# mu index
# getmail

# Enable and start services
# TLP
# kdbrate
